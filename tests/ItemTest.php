<?php

namespace Tests;

use PHPUnit\Framework\TestCase;
use App\Object\Item;

/**
 * Test Item.
 */
class ItemTest extends TestCase {

    /**
     * Test getters and setters in Item object.
     * PS: tests the trailing is stripped.
     *
     * @return void
     */
    public function testItem()
    {
        $item = new Item();
        $item->setTitle('Hello World!');
        $item->setUrl('https://helloworld.com/');
        $item->setSource('Google');

        $itemObject = $item->getItem();

        $this->assertSame([
                'title' => 'Hello World!',
                'url' => 'https://helloworld.com',
                'source' => 'Google',
            ], $itemObject
        );
    }
}
