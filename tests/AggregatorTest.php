<?php

namespace Tests;

use PHPUnit\Framework\TestCase;
use App\Aggregator;

/**
 * Test Aggregator.
 */
class AggregatorTest extends TestCase {
    public function testAggregate()
    {
        $aggregator = new Aggregator();
        $response = $aggregator->aggregate('Hello');
        $this->assertInternalType('array', $response);
        $this->assertArrayNotHasKey('error', $response);        
    }
}
