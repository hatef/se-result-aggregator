<?php

namespace Tests;

use PHPUnit\Framework\TestCase;
use App\Source\Bing;

/**
 * Test Bing.
 */
class BingTest extends TestCase {
    public function testQueryReturnsArray()
    {
        $bing = new Bing();
        $response = $bing->query('Hello');
        $this->assertInternalType('array', $response);
    }
}
