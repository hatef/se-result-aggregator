<?php

namespace Tests;

use PHPUnit\Framework\TestCase;
use App\Source\Google;

/**
 * Test Google.
 */
class GoogleTest extends TestCase {
    public function testQueryReturnsArray()
    {
        $google = new Google();
        $response = $google->query('Hello');
        $this->assertInternalType('array', $response);
    }
}
