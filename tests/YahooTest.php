<?php

namespace Tests;

use PHPUnit\Framework\TestCase;
use App\Source\Yahoo;

/**
 * Test Yahoo.
 */
class YahooTest extends TestCase {
    public function testQueryReturnsArray()
    {
        $yahoo = new Yahoo();
        $response = $yahoo->query('Hello');
        $this->assertInternalType('array', $response);
    }
}
