<?php

require 'vendor/autoload.php';
header('Content-Type: application/json');

use App\Aggregator;

$acceptedMethods = ["POST", "GET"];

if (!in_array($_SERVER['REQUEST_METHOD'], $acceptedMethods)) {
    // No other HTTP method is allowed
    http_response_code(405);
    echo json_encode(['error' => 'HTTP method not supported!']);
    exit();
}

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    $query = $_GET['q'] ?? $_GET['query'];
} else {
    foreach($_POST as $key => $value)
    {
        if (in_array($key, ['q', 'query'])) {
            $query = $value;
            break;
        }
    }
}

if (!isset($query)) {
    http_response_code(400);
    echo json_encode([
        'error' => 
            'You should specify a term for querying! You can use either "q" or "query" as a parameter'
        ]
    );
    exit();
}

try {
    $aggregator = new Aggregator();
    echo json_encode(
        $aggregator->aggregate($query)
    );
} catch(\Exception $e) {
    echo json_encode(['error' => 'Something went terribly wrong!']);
}
