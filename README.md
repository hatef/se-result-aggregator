# se-result-aggregator

- A search engine result aggregator library.
- Currently supported search engines: Google & Bing and partially Yahoo.
- Supported HTTP protocols: `GET` & `POST`
- Parameters to perform a query: You can use either `q` or `query` as a parameter in your `GET` or `POST` requests.


## Todos
- Include more tests in the test suite
- Find a way to improve Yahoo result aggregator
- Create an `.env` file and migrate the API keys there
- (optional) Store the results in a database.