<?php

namespace App;

/**
 * AbstractAggregator abstract class.
 */
abstract class AbstractAggregator {

    /**
     * Searches that this app queries from: 
     * Currently: Google, Bing, Yahoo.
     *
     * @var array
     */
    protected $sources = [
        'Yahoo' => Source\Yahoo::class,
        'Google' => Source\Google::class,
        'Bing' => Source\Bing::class,
    ];

    abstract protected function aggregate(string $query): array;
}
