<?php

namespace App;

use App\Source\Google;
use App\Source\Bing;
use App\Helper\Formatter;

/**
 * Aggregator class.
 * Aggregates the results from the specified search engines
 *
 * @package App
 */
class Aggregator extends AbstractAggregator {
    /**
     * Bootstraps the aggregation operation
     *
     * @param string $request
     * @return array
     */
    public function aggregate(string $query): array
    {
        $results = [];

        foreach ($this->sources as $source) {
            $result = (new $source)->query($query);
            empty($result) ? [] : array_push($results, ...$result);
        }

        return (new Formatter())->format($results) ?: ['error' => 'Nothing was found for your term!']; 
    }
}
