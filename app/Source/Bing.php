<?php

namespace App\Source;

use GuzzleHttp\Client;

/**
 * Bing class.
 * Performs a search using Bing search APIs
 */
class Bing extends Base implements SearchEngineInterface {

    /**
     * Bing API access key.
     */
    const BING_ACCESS_KEY = 'b40a35aeff6749c6a2dd823ffd9600a1';

    /**
     * Bing endpoint.
     */
    const BING_ENDPOINT = 'https://api.cognitive.microsoft.com/bing/v7.0/search';

    /** 
      * Query for the term and return process results
      *
      * @param string $term
      *
      * @return array
      */
      public function query(string $term): array {
        $items = [];
        $results = $this->getResults($term);

        if (!empty($results)) {
            $items = $this->processResults($results);
        }

        return $items;
    }

    /**
     * Get the results for queried term from the search engine
     *
     * @param string $term
     *
     * @return void
     */
    private function getResults($term): array {
        if (strlen(self::BING_ACCESS_KEY) == 32) {
            list($headers, $json) = $this->searchForTerm($term);
            $results = json_decode($json, true);

            return $results;
        } else {
            return [];
        }
    }

    /**
     * SearchForTerm: searches for the term using Bing API
     *
     * @param string $query
     *
     * @return array
     */
    private function searchForTerm(string $query): array {
        $key = self::BING_ACCESS_KEY;
        $headers = "Ocp-Apim-Subscription-Key: $key\r\n";
        $options =  [
                        'http' =>  [
                            'header' => $headers,
                            'method' => 'GET'
                        ]
                    ];

        $context = stream_context_create($options);
        $result = file_get_contents(self::BING_ENDPOINT . "?q=" . urlencode($query), false, $context);
        $headers = [];

        foreach ($http_response_header as $k => $v) {
            $h = explode(":", $v, 2);
            if (isset($h[1]))
                if (preg_match("/^BingAPIs-/", $h[0]) || preg_match("/^X-MSEdge-/", $h[0]))
                    $headers[trim($h[0])] = trim($h[1]);
        }

        return [$headers, $result];
    }

    /**
     * Processes the results returned from search engine
     *
     * @param array $results
     *
     * @return array $items
     */
    private function processResults(array $results): array {
        if (!isset($results['webPages']['value'])) {
            return [];
        }
        
        foreach ($results['webPages']['value'] as $item) {
            if (!$this->isValid($item['url']) || !$this->isValid($item['name'])) {
                continue;
            }

            $items[] = $this->processItem($item['url'], $item['name'], 'Bing');
        }

        return $items ?? [];
    }
}