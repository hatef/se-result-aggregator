<?php

namespace App\Source;

use GuzzleHttp\Client;

/**
 * Google class.
 * Performs a search using Google search APIs
 */
class Google extends Base implements SearchEngineInterface {

    /**
     * Google API access key.
     */
    const GOOGLE_ACCESS_KEY = 'AIzaSyA8KmfVqzscA42z6VAT_PRKGRQq827Jn5c';

    /**
     * Google custom search engine id.
     */
    const GOOGLE_CSE_ID = '003862522742988136439:uq6mvuv8f54';

    /**
     * Google endpoint.
     */
    const GOOGLE_ENDPOINT = 'https://www.googleapis.com/customsearch/';

    /** 
      * Query for the term and return process results
      *
      * @param string $term
      *
      * @return array
      */
    public function query(string $term): array {
        $results = $this->getResults($term);
        $items = $this->processResults($results);

        return $items;
    }

    /**
     * Get the results for queried term from the search engine
     *
     * @param string $term
     *
     * @return array
     */
    private function getResults($term): array {
        $key = self::GOOGLE_ACCESS_KEY;
        $cseId = self::GOOGLE_CSE_ID;

        $client = new Client([
            // Base URI is used with relative requests
            'base_uri' => self::GOOGLE_ENDPOINT,
            // You can set any number of default request options.
            'timeout'  => 2.0,
        ]);

        try {
            $response = $client->request('GET', "v1?key={$key}&cx={$cseId}&q={$term}");
            return json_decode($response->getBody()->getContents(), true);
        } catch (\Exception $e) {
            return [];
        }
    }
    
    /**
     * Processes the results returned from search engine
     *
     * @param array $results
     *
     * @return array $items
     */
    private function processResults(array $results): array {        
        if (!isset($results['items'])) {
            return [];
        }

        foreach ($results['items'] as $item) {
            if (!$this->isValid($item['link']) || !$this->isValid($item['title'])) {
                continue;
            }

            $items[] = $this->processItem($item['link'], $item['title'], 'Google');
        }

        return $items ?? [];
    }
}
