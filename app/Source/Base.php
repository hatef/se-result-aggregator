<?php

namespace App\Source;

use App\Object\Item;

/**
 * Base class.
 */
class Base {

    /**
     * Processes one item in the returned results from search engine
     *
     * @param array $item
     *
     * @return Item
     */
    protected function processItem(string $link, string $title, string $source): Item {
        $itemObject = new Item();

        $itemObject->setUrl($link);
        $itemObject->setTitle($title);
        $itemObject->setSource($source);

        return $itemObject;
    }

    /**
     * A generic validator for a parameter.
     *
     * @param string $param
     * @return bool
     */
    protected function isValid(string $param = null): bool {
        return (isset($param) && !empty($param));
    }
}