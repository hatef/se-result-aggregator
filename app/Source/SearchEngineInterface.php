<?php

namespace App\Source;

use App\Object\Item;

/**
 * SearchEngineInterface interface.
 */
interface SearchEngineInterface {

    /**
     * Query function.
     * Every search engine must have a query function that returns an array.
     *
     * @param string $query
     *
     * @return array
     */
    public function query(string $query): array;
}
