<?php

namespace App\Source;

use GuzzleHttp\Client;

/**
 * Yahoo class.
 * Performs a search using Yahoo search engine
 */
class Yahoo extends Base implements SearchEngineInterface {
    
    /** 
      * Query for the term and return process results
      *
      * @param string $term
      *
      * @return array
      */
    public function query(string $term): array {
        $results = $this->getResults($term);
        $items = $this->processResults($results);

        return $items;
    }

    /**
     * Get the results for queried term from the search engine
     *
     * @param string $term
     *
     * @return array $results
     */
    private function getResults(string $term): array {
        $request =  "https://search.yahoo.com/search?p={$term}&pz=10&bct=0&b=1&pz=10&bct=0&xargs=0";
        $htmlResult = file_get_contents($request);
        $resultsSection = strstr(
                    strstr($htmlResult, '<div class="dd algo algo-sr fst'),
                    '<div class="dd algo algo-sr lst Sr',
                    true
                );

        list(, $results[0], $results[1], $results[2], $results[3], $results[4], $results[5], $results[6], $results[7], $results[8]) 
            = explode("a class=\" ac-algo fz-l ac-21th lh-24\"", $resultsSection);

        return $results;
    }

    /**
     * Processes the results returned from search engine
     *
     * @param array $results
     *
     * @return array $items
     */
    private function processResults(array $results): array {
        foreach($results as $item) {
            if (preg_match('/\"(.*?)\"/', $item, $match) == 1) {
                $link = $match[1];
            }
    
            if (preg_match('/\>(.*?)<\/a>/', $item, $match) == 1) {
                $title = $match[1];
            }

            if (!$this->isValid($link) || !$this->isValid($title)) {
                continue;
            }

            $items[] = $this->processItem($link, $title, 'Yahoo');
        }

        return $items ?? [];
    }
}
