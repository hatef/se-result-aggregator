<?php

namespace App\Object;

/**
 * Item class.
 * Maps one item from any search engine to an object with url, title & source
 */
class Item {

    /**
     * URL of the item
     *
     * @var string $url
     */
    protected $url;

    /**
     * Title of the item
     *
     * @var string $title
     */
    protected $title;

    /**
     * Search engine
     *
     * @var string $source
     */
    protected $source;

    public function setUrl(string $url) {
        $this->url = $url;
    }

    public function setTitle(string $title) {
        $this->title = $title;
    }

    public function setSource(string $source) {
        $this->source = $source;
    }

    public function getItem() {
        return [
            'title' => $this->getTitle(),
            'url' => rtrim($this->getUrl(),'/'),
            'source' => $this->getSource()
        ];
    }

    public function getUrl() {
        return rtrim($this->url, '/');
    }

    public function getTitle() {
        return $this->title;
    }

    public function getSource() {
        return $this->source;
    }
}
