<?php

namespace App\Helper;

/**
 * Formatter class.
 */
class Formatter {

    /**
     * Formats the results returned from all search engines
     *
     * For duplicate items, it combines them and only keeps the source (search engine)
     * under an array named source inside the item.
     *
     * @param array $items
     *
     * @return array
     */
    public function format(array $items): array {
        $returnArray = [];

        foreach ($items as $item) {

            if (array_key_exists($item->getUrl(), $returnArray)) {
                if (is_array($returnArray[$item->getUrl()]['source'])) {
                    $returnArray[$item->getUrl()]['source'][] = $item->getSource();
                } else {                
                    $returnArray[$item->getUrl()]['source'] = [
                        $returnArray[$item->getUrl()]['source'],
                        $item->getSource()
                    ];
                }
                continue;
            }

            $returnArray[$item->getUrl()] = $item->getItem();
        }

        return array_values($returnArray);
    }
}
